Source: python-eliot
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Andrej Shadura <andrewsh@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-boltons,
 python3-hypothesis,
 python3-pyrsistent,
 python3-setuptools,
 python3-testtools,
 python3-zope.interface
Standards-Version: 4.6.2
Homepage: https://github.com/itamarst/eliot/
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python
Vcs-Browser: https://salsa.debian.org/python-team/packages/eliot
Vcs-Git: https://salsa.debian.org/python-team/packages/eliot.git

Package: python3-eliot
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${python3:Depends}
Description: logging library for Python that tells you why things happen
 Most logging systems tell you what happened in your application,
 whereas eliot also tells you why it happened.
 .
 eliot is a Python logging system that outputs causal chains of actions:
 actions can spawn other actions, and eventually they either succeed or fail.
 The resulting logs tell you the story of what your software did: what
 happened, and what caused it.
 .
 Eliot works well within a single process, but can also be used across
 multiple processes to trace causality across a distributed system.
 .
 Eliot is only used to generate your logs; you will still need tools like
 Logstash and ElasticSearch to aggregate and store logs if you are using
 multiple processes.
